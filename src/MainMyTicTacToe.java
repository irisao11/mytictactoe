import java.util.Scanner;

public class MainMyTicTacToe {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("jucatorul 1 este: ");
        String player1Name = s.nextLine();
        System.out.println("jucatorul 2 este: ");
        String player2Name = s.nextLine();

        Player player1 = new Player(player1Name, MyTicTacToe.SYMBOL_X);
        Player player2 = new Player(player2Name, MyTicTacToe.SYMBOL_0);

        MyTicTacToe myTicTacToe = new MyTicTacToe(player1, player2);
        myTicTacToe.playGame();

    }


}
