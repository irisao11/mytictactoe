import java.sql.SQLOutput;
import java.util.Scanner;

public class MyTicTacToe {
    public static final char SYMBOL_X = 'X';
    public static final char SYMBOL_0 = '0';
    public static final int SIZE = 3;

    char[][] game;

    Player player1;
    Player player2;

    public MyTicTacToe(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.game = new char[SIZE][SIZE];
    }

    void initBoard() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                Integer boardValue = SIZE * i + j + 1;
                game[i][j] = boardValue.toString().charAt(0);// functioneaza doar pentru matrice de 3 pe 3, care au doar 9 pozitii

            }

        }
    }

    void showBoard() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                System.out.print(game[i][j] + " ");
            }
            System.out.println();
        }
    }

    Move readMove(Player player) {
        Scanner s = new Scanner(System.in);
        System.out.println(player.name + " alegeti pozitia");
        int moveIndex = s.nextInt();// aloritm de transformare a nr din matrice in index i,j
        int line = (moveIndex - 1) / SIZE; // Scadem 1 pt ca l-am adaugat la initializarea boardului
        int col = (moveIndex - 1) % SIZE;
        return new Move(line, col);
    }

    void makeMove(Move move, char symbol) {
        game[move.line][move.col] = symbol;//facem mutarea pe care a ales-o jucatorul. game[][]reprezinta matricea
    }//move.line inseamna linia obiectolui move de tip Move, creat in readMove.

    boolean isWinLine(Move move, char symbol) {
        boolean isWinLine = true;
        int j = 0;
        while (j < SIZE && isWinLine) {
            if (game[move.line][j] != symbol) {
                isWinLine = false;
            }
            j++;
        }
        return isWinLine;
    }

    boolean isWinCol(Move move, char symbol) {
        boolean isWinCol = true;
        int i = 0;
        while (i < SIZE && isWinCol) {
            if (game[i][move.col] != symbol) {
                isWinCol = false;
            }
            i++;
        }
        return isWinCol;
    }

    boolean isWinDiag1(Move move, char symbol) {
        boolean isWinDiag1 = true;
        int i = 0;
        while (i < SIZE) {
            if (game[i][i] != symbol) {
                isWinDiag1 = false;
            }
            i++;
        }
        return isWinDiag1;
    }

    boolean isWinDiag2(Move move, char symbol) {
        boolean isWinDiag2 = true;
        int i = 0;
        while (i < SIZE) {
            if (game[i][SIZE - 1 - i] != symbol) {
                isWinDiag2 = false;
            }
            i++;
        }
        return isWinDiag2;
    }

    boolean testisWin(Move move, char symbol) {
        boolean isWin = false;
        // test line
        isWin = isWinLine(move, symbol);
        //test col
        if (!isWin) { //testez mai departe doar daca nu sunt linii castigatoare
            isWin = isWinCol(move, symbol);
        }
        // test diag 1 if required
        if (!isWin && move.line == move.col) {
            isWin = isWinDiag1(move, symbol);
        }
        //test diag2 if required
        if (!isWin && (move.line + move.col + 1 == SIZE)) {
            isWin = isWinDiag2(move, symbol);
        }
        return isWin;
    }

    boolean validateSize(Move move) {
        boolean validSize = true;
        if (move.line > (SIZE - 1) || move.col > (SIZE - 1)) {
            validSize = false;
        }
        return validSize;
    }

    boolean vadidatePosition(Move move) {
        boolean validposition = true;
        if (game[move.line][move.col] == SYMBOL_X || game[move.line][move.col] == SYMBOL_0) {
            validposition = false;
        }
        return validposition;
    }

    boolean testValidation(Move move) {
        boolean isValid = false;
        if (validateSize(move) && vadidatePosition(move)) {
            isValid = true;
        }

        return isValid;
    }

    public void playGames() {
        Scanner s = new Scanner(System.in);
        System.out.println(" Another game?");
        String newGame = s.nextLine();
        if (newGame.equals("da")) {
            System.out.println(" Incepeti un nou joc");
            playGame();
        } else {
            System.out.println(" Good bye!");
        }
    }


    public void playGame() {
        initBoard();
        showBoard();
        Player currentPlayer = player1;
        boolean isWin = false;
        boolean areMoves = true;
        int nrMoves = 0;
        while (!isWin && areMoves) {
            Move move = null;
            boolean isValid = false;
            while (!isValid) {
                move = readMove(currentPlayer);
                isValid = testValidation(move);//???????????
            }
            makeMove(move, currentPlayer.symbol);
            showBoard();
            nrMoves++;
            if (nrMoves == SIZE * SIZE) {
                areMoves = false;
            }
            //NUMARUL MINIM DE MUTARI DUPA CARE JOCUL POATE AVEAM UN CASTIATOR ESTE 5
            if (nrMoves >= SIZE + SIZE - 1) {
                isWin = testisWin(move, currentPlayer.symbol);//testez daca avem castigator
            }
            if (!isWin) {
                if (currentPlayer == player1) {
                    currentPlayer = player2;
                } else {
                    currentPlayer = player1;
                }
            }
        }

        if (isWin) {
            System.out.println(currentPlayer.name + " is the winner!!");

        } else {
            System.out.println(" draw game");
        }
        playGames();

    }


}
